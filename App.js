import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View ,  Alert} from 'react-native';

export default function App() {

  state = {
    mystate: 'Hello worlds'
  }

  updateState = () => this
  return (
    <View style={styles.container}>
      <Text onPress={this.updateState}>{this.state.mystate}</Text>
      <StatusBar style="auto" />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
